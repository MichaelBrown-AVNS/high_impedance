# -*- coding: utf-8 -*-
"""
Created on Tue Jul 20 10:32:33 2021

@author: 610250
"""

import PySimpleGUI as sg
from time import sleep
import nidaqmx 









def main():
    layout = [ [sg.Text("Starting Pump Voltage:"), sg.InputText(default_text = "0.8", key = "-StartingVoltage-")],
               [sg.Text("Ending Pump Voltage:"), sg.InputText(default_text = "2.5", key = "-EndingVoltage-")],
               [sg.Text("Ramp Time:"), sg.InputText(default_text = "20", key = "-RampTime-")],
               [sg.Text("Current Pump Voltage:"), sg.Text(text = "0.00", key = "-PumpVoltage-")],
               [sg.Button("Start Priming"), sg.Button("Start Ramping"), sg.Button("Stop Pump"), sg.Button("Kill Program")]
              ]
    
    window = sg.Window("Pump Ramp Tool", layout)
    previousPumpVoltage = -1
    pumpVoltage = 0
    pumpOn = False
    rampVoltage = False
    rampRateFloat = -1
    nextVoltage = -1
    endVoltage = -1
    
    sleepTime = 100 # in ms
    delayAdjustment = 0.13333 # adjust for observed delay
    
    continueLoop = True
    
    task = nidaqmx.Task()
    task.ao_channels.add_ao_voltage_chan("Dev1/ao0")
    task.start()
    
    
    while continueLoop:
        event, values = window.read(timeout = sleepTime)
        if event == sg.WIN_CLOSED or event == "Kill Program":
            continueLoop = False
            continue
        
        if event == "Start Priming":
            pumpVoltage = float(values["-StartingVoltage-"])
            pumpOn = True
            rampVoltage = False
        
        if event == "Start Ramping":
            pumpVoltage = float(values["-StartingVoltage-"])
            nextVoltage = pumpVoltage
            pumpOn = True
            rampVoltage = True
            endVoltage =float(values["-EndingVoltage-"])
            rampRateFloat = (endVoltage - pumpVoltage) / float(values["-RampTime-"]) * (1 + delayAdjustment)
            
        if event == "Stop Pump":
            pumpVoltage = 0
            pumpOn = False
            rampVoltage = False
            previousPumpVoltage = -1
            rampRateFloat = -1
            nextVoltage = -1
            endVoltage = -1
        
        if pumpOn:
            if not rampVoltage:
                if not (pumpVoltage == previousPumpVoltage):
                    print("Start the Pump at " + '%.2f'%(pumpVoltage))
                    window["-PumpVoltage-"].update('%.2f'%(pumpVoltage))
                    ## add daq command to start pump
                    task.write(pumpVoltage)
                    previousPumpVoltage = pumpVoltage
                    continue
                else:
                    print("Pump already running at correct voltage: " + '%.2f'%(pumpVoltage))
                    continue
            else:
                if (nextVoltage>=endVoltage):
                    nextVoltage = endVoltage
                else:
                    nextVoltage = nextVoltage + rampRateFloat * sleepTime/1000
                pumpVoltage = nextVoltage
                
                if not (previousPumpVoltage == pumpVoltage):
                    print("Ramping the Pump. Next Voltage: " + '%.2f'%(pumpVoltage))
                    window["-PumpVoltage-"].update('%2f'%(pumpVoltage))
                    ## add daq command to change pump voltage
                    task.write(pumpVoltage)
                    previousPumpVoltage = pumpVoltage
                    continue
                else:
                    print("Pump already running at correct voltage : " + '%.2f'%(pumpVoltage))

        else:
            print("Turning Pump Off: " + '%.2f'%(pumpVoltage))
            window["-PumpVoltage-"].update('%.2f'%(pumpVoltage))
            # turn pump off
            task.write(0)
    
    window.close()
    task.stop()
    task.close()




if __name__ == '__main__':
    main()